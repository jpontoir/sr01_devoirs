#include <stdio.h>
#include <stdlib.h>
#define TAILLE 7

float* ajouter_notes(int N) {
	float *POINTS;
	if(N > 0) {
		POINTS = (float*)malloc(N * sizeof(float));
		for(int i = 0; i < N; i++) {
		printf("Indiquez la note de l'eleve %d au devoir de SR01\n", i+1);
		scanf("%f", &POINTS[i]);
		}
	} else {
		printf("Vous devez entrer la note d'au moins un eleve !");
	}
	return POINTS;
}

void max_notes(float *POINTS, int N) {
	float max = POINTS[0];
	for (int i=0; i<N; i++) {
		if (max < POINTS[i]) {
			max = POINTS[i];
		}
	}
	printf("La note maximale est de %f.\n", max);
}


void min_notes(float *POINTS, int N) {
	float min = POINTS[0];
	for (int i=0; i<N; i++) {
		if (min > POINTS[i]) {
			min = POINTS[i];
		}
	}
	printf("La note minimale est de %f.\n", min);
}


void moyenne_notes(float *POINTS, int N) {
	float moy = 0;
	for(int i=0; i<N; i++) {
		moy += POINTS[i];
	}
	moy /= N;
	printf("La moyenne est de %f.\n", moy);
}

int* tableau(float *POINTS, int N) {
	int* NOTES =(int*)malloc(7*sizeof(int));

	for(int j=0; j<7; j++) {
		NOTES[j] = 0;
	}

	for(int i=0; i<N; i++) {
		if(POINTS[i] == 60)
			NOTES[6]++;
		else if(POINTS[i]>=50)
			NOTES[5]++;
		else if(POINTS[i]>=40)
			NOTES[4]++;
		else if(POINTS[i]>=30)
			NOTES[3]++;
		else if(POINTS[i]>=20)
			NOTES[2]++;
		else if(POINTS[i]>=10)
			NOTES[1]++;
		else if(POINTS[i]>=0)
			NOTES[0]++;
	}
	return NOTES;
}

void graph_points(int MAXN, int* NOTES) {
	for(int i = MAXN; i >= 1; i--) {
		printf("%4d >    ", i);
		for(int j=0; j<TAILLE; j++) {
			printf("    ");
			if(NOTES[j] == i) {
				printf("o");
			} else {
				printf(" ");
			}
			printf("   ");
		}
		printf("\n");
	}
	printf("          ");
	for(int j=0; j<TAILLE; j++) {
		printf("+---");
		if(NOTES[j] == 0) {
			printf("o");
		} else {
			printf("-");
		}
		printf("---");
	}
	printf("+\n");
	printf("          ");
	for(int j=0; j<TAILLE-1; j++) {
		printf(" |%2d-%2d|", 10*j, 10*j+9);
	}
	printf(" | 60 | \n");
}


void graph_batons(int MAXN, int* NOTES) {
	for(int i = MAXN; i >= 1; i--) {
		printf("\n%4d >    ", i);
		for(int j=0; j<TAILLE; j++) {
			printf("  ");
			if(NOTES[j] >= i) {
				printf("#####");
			} else {
				printf("     ");
			}
			printf(" ");
		}
		printf("\n");
	}
	printf("          ");
	for(int j=0; j<TAILLE; j++) {
		printf("+-------");
	}
	printf("+\n");
	printf("          ");
	for(int j=0; j<TAILLE-1; j++) {
		printf(" |%2d-%2d|", 10*j, 10*j+9);
	}
	printf(" | 60 | \n");
}


int main() {
	float* POINTS;
	int* NOTES;
	int N, MAXN;
	printf("Indiquez le nombre de notes a entrer\n");
	scanf("%d", &N);
	if(N <= 0) {
		printf("Vous devez entrer la note d'au moins un eleve !");
		return -1;
	}
	POINTS  = ajouter_notes(N);
	max_notes(POINTS, N);
	min_notes(POINTS, N);
	moyenne_notes(POINTS, N);
	NOTES = tableau(POINTS, N);
	for(int i=0; i<TAILLE; i++) {
		printf("%d\n", NOTES[i]);
	}

	MAXN = NOTES[0];

	for(int i = 1; i<TAILLE; i++) {
		if(NOTES[i] > MAXN) MAXN = NOTES[i];
	}
	printf("\n");
	printf("%d\n", MAXN);

	graph_points(MAXN, NOTES);
	graph_batons(MAXN, NOTES);


	free(POINTS);
	free(NOTES);
}