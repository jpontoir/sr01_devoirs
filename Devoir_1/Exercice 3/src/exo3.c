#include "exo3.h"


int lire_restaurant(char* chemin, Restaurant restaurants []) {
	int nb_ligne = 0, colonne = 0, verif_adr = 0, verif_coord = 0, nb_restau = 0;
    char c, nom[TAILLE_MAX], numero[TAILLE_MAX], rue[TAILLE_MAX], ville[TAILLE_MAX], pos_x[TAILLE_MAX], pos_y[TAILLE_MAX], specialite[TAILLE_MAX];
    double x = 0, y = 0;
    strcpy(nom, "");
    strcpy(numero, "");
    strcpy(rue, "");
    strcpy(ville, "");
    strcpy(pos_x, "");
    strcpy(pos_y, "");
    strcpy(specialite, "");

	FILE* fichier = fopen(chemin, "r"); // ouverture du fichier en lecture pour récupérer les données

    if(fichier != NULL) {
        while((c = fgetc(fichier)) != EOF) { // on récupère caractère par caractère
            if(c == '\n') { // fin de ligne
                if(nb_ligne < 3) { //les 3 premières lignes ne contiennent pas de données utiles
                    nb_ligne++; 
                } else { // permet de mettre les données dans le tableau de restaurants
                    if(nb_ligne%2 !=0) {
                        Adresse *new_adr = malloc(sizeof(Adresse));
                        Position *new_pos = malloc(sizeof(Position));
                        Restaurant *new_restau = malloc(sizeof(Restaurant));
                        //on alloue l'espace nécessaire pour les 3 structures

                        new_adr->num = strdup(numero); // strdup permet d'allouer de la mémoire et créer une copie de la variable en paramètre
                        new_adr->rue = strdup(rue);
                        new_adr->ville = strdup(ville);

                        new_pos->x = atof(pos_x); // transformation en float
                        new_pos->y = atof(pos_y);

                        new_restau->nom_restaurant = strdup(nom);
                        new_restau->adresse_restaurant = new_adr;
                        new_restau->position_restaurant = new_pos;
                        new_restau->specialite = strdup(specialite);

                        restaurants[nb_restau] = *new_restau;
                        nb_restau++; // incrémente le nombre de restaurant
                    }

                    x = 0;
                    y = 0;
                    strcpy(nom, "");
                    strcpy(numero, "");
                    strcpy(rue, "");
                    strcpy(ville, "");
                    strcpy(pos_x, "");
                    strcpy(pos_y, "");
                    strcpy(specialite, "");
                    colonne = 0;
                    verif_adr = 0;
                    verif_coord = 0;
                    
                    nb_ligne++;

                    // remettre à zéro (nouvelle ligne fdp)
                }
            } 
            if(colonne == 1) { // adresse du restaurant
                if(c == ',') { //passage dans la 2ème partie de l'adresse (rue)
                    verif_adr = 1; //compteur pour indiquer dans quelle partie on se trouve
                } else if(c == '-') { // passage dans la 3ème partie de l'adresse (ville)
                    verif_adr = 2;
                }
            } else if((c == ',') && (colonne == 2)) { // passage dans la deuxième partie des coordonnées (y)
                verif_coord = 1;
            }
            if((nb_ligne%2 != 0) && (nb_ligne > 1)) { // changement de colonne
                if(c == ';') {
                    colonne++;
                }
                else if(colonne == 0 && c != '\n') { // vérification qu'on ne se trouve pas dans une ligne vide et ajout du nom
                    strncat(nom, &c, 1); // ajout du caractère à la fin de la variable c
                }
                else if(colonne == 1) { // dans l'adresse
                    if(verif_adr == 0) { // dans le numero
                        strncat(numero, &c, 1);
                    } else if(verif_adr == 1) { // dans la rue
                        strncat(rue, &c, 1);
                    } else { // dans la ville
                        strncat(ville, &c, 1);
                    }
                }
                else if((colonne == 2) && ((isdigit(c)) || (c == '.'))) { //récupération des valeurs numériques des coordonnées
                    if(verif_coord == 0) { // dans x
                        strncat(pos_x, &c, 1);
                    } else { // dans y
                        strncat(pos_y, &c, 1);
                    }
                }
                else if(colonne == 3){ // dans spécialité
                    if(c == '{') strcpy(specialite, ""); // permet de supprimer l'espace qui va s'ajouter avant le '{'
                    if((c != '{') && (c != '}') && (c != '\n')) { // permet de récupérer la spécialité
                        strncat(specialite, &c, 1);
                    }
                }
            }
        }

    } else {
        printf("Impossible d'ouvrir le fichier.");
        return -1;
    }

    fclose(fichier);
    return nb_restau;
}

int inserer_restaurant(Restaurant restaurant) {

    FILE *fichier = fopen("restau.txt", "a"); // ouverture du fichier en append (ajout à la fin du fichier)

    if(fichier) {    
        fprintf(fichier, "\n%s; %s%s %s; (x=%lf, y=%lf); {%s};\n", restaurant.nom_restaurant, restaurant.adresse_restaurant->num, restaurant.adresse_restaurant->rue, restaurant.adresse_restaurant->ville, restaurant.position_restaurant->x, restaurant.position_restaurant->y, restaurant.specialite);
        //ajout du restaurant dans la liste en respectant le formatage du fichier
    } else {
        return 0;
    }
    fclose(fichier);
    return 1;
}

void cherche_restaurant(double x, double y, double rayon_recherche, Restaurant results[]) {
    Restaurant liste[TAILLE_MAX];
    int nb = lire_restaurant("restau.txt", liste), c = 0; // appel de lire_restaurant pour récupérer un tableau de tous les restaurants
    double calcul;

    for(int i=0; i<nb; i++){
        calcul = sqrt(pow((liste[i].position_restaurant->x - x), 2) + pow((liste[i].position_restaurant->y - y), 2)); // calcul de la distance
        if(calcul <= rayon_recherche) {
            results[c] = liste[i]; //ajout du restaurant dans la liste
            printf("Restaurant %d:\n", c + 1); // affichage
            printf("Nom: %s\n", results[c].nom_restaurant);
            printf("Adresse: %s %s %s\n", results[c].adresse_restaurant->num, results[c].adresse_restaurant->rue, results[c].adresse_restaurant->ville);
            printf("Position: (%lf, %lf)\n", results[c].position_restaurant->x, results[c].position_restaurant->y);
            printf("Specialite: %s\n", results[c].specialite);
            printf("\n");
            c++;
        }
    }
}


void cherche_par_specialite(double x, double y, char * specialite[], Restaurant results[], int taille_spe) {
    Restaurant liste[TAILLE_MAX];
    int nb = lire_restaurant("restau.txt", liste), c = 0;
    int min;

    Restaurant temp;

    for(int i = 0; i< nb; i++) {
        for(int j = 0; j < taille_spe; j++) {
            if(strcasecmp(liste[i].specialite, specialite[j]) == 0) { //compare si ce sont les mêmes specialités
                results[c] = liste[i]; // ajoute dans le tableau results si c'est le cas
                c++;
            }
        }
    }
    for(int i = 0; i < c; i++) { // algo de tri à bulle
        for(int j = 0; j < c-1; j++) {
            if(sqrt(pow((results[j].position_restaurant->x - x), 2) + pow((results[j].position_restaurant->y - y), 2)) > (sqrt(pow((results[j+1].position_restaurant->x - x), 2) + pow((results[j+1].position_restaurant->y - y), 2)))) {
                temp = results[j];
                results[j] = results[j+1];
                results[j+1] = temp;
            }
        }

    }
    
    for(int k=0; k<c; k++) { // affichage
        printf("Restaurant %d:\n", k+1);
        printf("Nom: %s\n", results[k].nom_restaurant);
        printf("Adresse: %s %s %s\n", results[k].adresse_restaurant->num, results[k].adresse_restaurant->rue, results[k].adresse_restaurant->ville);
        printf("Position: (%lf, %lf)\n", results[k].position_restaurant->x, results[k].position_restaurant->y);
        printf("Specialite: %s\n", results[k].specialite);
        printf("\n");
    }
}


/* --------------------------------- Fonctions utilitaires -------------------------------------- */

void viderBuffer() { // pour éviter les problèmes lors de l'utilisation de getchar()
    int c = 0;
    while (c != '\n' && c != EOF) {
        c = getchar();
    }

}

int lire(char *chaine, int longueur) // permet de supprimer le \n d'un texte récupérer par un fgets()
{
    char* pos = NULL;
 
    if (fgets(chaine, longueur, stdin) != NULL)  // Test si erreur de saisie
    {
        pos = strchr(chaine, '\n');
        if (pos != NULL)
        {
            *pos = '\0';
        }
        return 1;
    } else{
        return 0;
    }
}