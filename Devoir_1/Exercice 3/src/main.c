#include "exo3.h"


int main() {
    int nb, t, n;
    char chemin[TAILLE_MAX], nom[TAILLE_MAX], numero[TAILLE_MAX], rue[TAILLE_MAX], ville[TAILLE_MAX], specialite[TAILLE_MAX], comp[TAILLE_MAX] = ", ", comp2[TAILLE_MAX] = "- ", temp[TAILLE_MAX];
    double x, y, dist;

    char choix = '0';
    while(choix != '5') { // affichage du menu
        printf("\n======================================");
        printf("\n1. Lire un tableau de Restaurants");
        printf("\n2. Inserer un Restaurant");
        printf("\n3. Chercher un Restaurant (position)");
        printf("\n4. Chercher un Restaurant (specialite)");
        printf("\n5. Quitter");
        printf("\n======================================");
        printf("\n   Votre choix ? ");

        choix = getchar(); // récupération du choix de l'utilisateur
        viderBuffer();

        switch(choix) {
            case '1':
                Restaurant resto[TAILLE_MAX];
                printf("Veuillez entrer le chemin du fichier a lire : \n");
                scanf("%s", chemin);
                viderBuffer();
                printf("%s", chemin);
                nb = lire_restaurant(chemin, resto);
                if (nb > 0) {
                    printf("Le fichier contient %d restaurants.\n", nb);
                    for (int i = 0; i < nb; i++) {
                        printf("Restaurant %d:\n", i + 1);
                        printf("Nom: %s\n", resto[i].nom_restaurant);
                        printf("Adresse: %s %s %s\n", resto[i].adresse_restaurant->num, resto[i].adresse_restaurant->rue, resto[i].adresse_restaurant->ville);
                        printf("Position: (%lf, %lf)\n", resto[i].position_restaurant->x, resto[i].position_restaurant->y);
                        printf("Specialite: %s\n", resto[i].specialite);
                        printf("\n");
                    }
                } else {
                    printf("Erreur a l'ouverture du fichier !\n");
                }
                break;

            case '2':
                printf("Veuillez entrer le nom :\n");
                lire(nom, TAILLE_MAX);
                printf("Veuillez entrer le numero :\n");
                lire(numero, TAILLE_MAX);
                printf("Veuillez entrer la rue :\n");
                lire(rue, TAILLE_MAX);
                strcat(comp, rue);
                printf("Veuillez entrer la ville (et l'arrondissement s'il y a) :\n");
                lire(ville, TAILLE_MAX);
                strcat(comp2, ville);
                printf("Veuillez entrer les coordonnees (x puis y)\n");
                scanf("%lf", &x);
                viderBuffer();
                scanf("%lf", &y);
                viderBuffer();
                printf("Veuillez entrer la specialite :\n");
                lire(specialite, TAILLE_MAX);
                
                // création du restaurant
                Restaurant new_resto;
                new_resto.nom_restaurant = nom;
                new_resto.adresse_restaurant = malloc(sizeof(Adresse));
                new_resto.adresse_restaurant->num = numero;
                new_resto.adresse_restaurant->rue = comp;
                new_resto.adresse_restaurant->ville = comp2;
                new_resto.position_restaurant = malloc(sizeof(Position));
                new_resto.position_restaurant->x = x;
                new_resto.position_restaurant->y = y;
                new_resto.specialite = specialite;


                t = inserer_restaurant(new_resto); // insertion dans le fichier 'restau.txt'

                strcpy(comp, ", "); // ajout du formatage du fichier
                strcpy(comp2, "- "); // ajout du formatage du fichier 

                if(t == 0) {
                    printf("Erreur insertion.");
                } else {
                    printf("Restaurant insere.");
                }

                break;

            case '3':
                Restaurant results[TAILLE_MAX];

                printf("Veuillez entrer les coordonnees (x puis y)\n");
                scanf("%lf", &x);
                viderBuffer();
                scanf("%lf", &y);
                viderBuffer();
                printf("Veuillez entrer la distance maximale\n");
                scanf("%lf", &dist);
                viderBuffer();
                printf("\n");

                if(dist > 0) { // vérification que la distance est positive
                    cherche_restaurant(x, y, dist, results);
                } else {
                    printf("Erreur, distance négative !");
                }
                

                break;

            case '4':
                printf("Veuillez entrer les coordonnees (x puis y)\n");
                scanf("%lf", &x);
                viderBuffer();
                scanf("%lf", &y);
                viderBuffer();
                printf("Combien de specialites recherchez-vous ?\n");
                scanf("%d", &n);
                viderBuffer();

                char* spe2[TAILLE_MAX];
                Restaurant results2[TAILLE_MAX];

                for(int i=0; i<n; i++) { // récupération des spécialités
                    printf("Veuillez entrer la specialite numero %d\n", i+1);
                    lire(temp, TAILLE_MAX);
                    strcpy(spe2[i], temp);
                }

                printf("\n");
                cherche_par_specialite(x, y, spe2, results2, n);
                break;

            case '5':
                printf("\n======== PROGRAMME TERMINE ========\n");
                break;

            default :
                printf("\n\nERREUR : votre choix n'est valide ! ");

        }
    }
    return 0;
}