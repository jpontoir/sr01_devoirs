#ifndef EXO3_H
#define EXO3_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#define TAILLE_MAX 100


typedef struct Adresse {
	char* num;
	char* rue;
	char* ville;
} Adresse;

typedef struct Position {
	double x;  // utilisation de double qui permet de stocker des nombres decimaux avec une meilleure precision qu'un float, utile pour stocker des coordonnees
	double y;
} Position;

typedef struct Restaurant {
	char* nom_restaurant;
	Adresse* adresse_restaurant;
	Position* position_restaurant;
	char* specialite;
} Restaurant;

int lire_restaurant(char* chemin, Restaurant restaurants[]);
int inserer_restaurant(Restaurant restaurant);
void cherche_restaurant(double x, double y, double rayon_recherche, Restaurant results[]);
void cherche_par_specialite(double x, double y, char * specialite[], Restaurant results[], int taille_spe);
void viderBuffer();
int lire(char *chaine, int longueur);

#endif