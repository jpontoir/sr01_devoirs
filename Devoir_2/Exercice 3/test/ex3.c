#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

void ecrire_fils(int nb, char* name) {
    FILE *file = fopen(name, "w"); //Ouverture du fichier en écriture

    if (!file) { //Si erreur on arrete
        perror("\n\nErreur lors de l'ouverture du fichier (ecriture)\n");
        fclose(file);
        return;
    } else { //Sinon

        fprintf(file, "%d", nb); //ON ecrit le nombre passé en paramètre.
        fclose(file); //Fermeture
    }
}

void lire_pere(int* nb, char* name) {

    FILE *file = fopen(name, "r"); // Ouvre le fichier en mode lecture

    if (!file) {//Si erreur on arrete
        perror("\n\nErreur lors de l'ouverture du fichier (lecture)\n");
        return;
    }

    if (fscanf(file, "%d", nb) != 1) { //On lit le fichier pour chercher le nombre
        //Si le scanf != 1 on affiche erreur de lecture
        perror("Erreur lors de la lecture de l'entier dans le fichier.\n");
        return;
    }

    fclose(file);  //Fermeture

    if (remove(name) != 0) { //On supprime le fichier
        perror("Erreur suppression fichier\n");
    }
}

int maxi(int i, int j) {
  return (i < j) ? j : i;
}

int max(int* tab, int debut, int fin){
	int max = tab[debut];
	for(int i=debut+1; i<=fin; i++){
		if(tab[i] > max) {
      max = tab[i];
    }
	}
	return max;
}

int recherche(int* tab, int debut, int fin, int seuil) {
    if (fin - debut + 1 <= seuil) {
        // Effectue une recherche séquentielle si le tableau est de taille inférieure au seuil
        return max(tab, debut, fin);
    } else {
        // Divise le travail en deux parties et crée deux processus fils pour chercher le maximum
        int mid = (debut + fin) / 2;
        int pid1 = fork();
        int pid2;
        int max1, max2;

        if (pid1 == 0) { // Processus fils 1
            max1 = recherche(tab, debut, mid, seuil);
            ecrire_fils(max1, "temp1.txt");
            exit(EXIT_SUCCESS);
        } else { // Processus père
            pid2 = fork();
            if (pid2 == 0) { // Processus fils 2
                max2 = recherche(tab, mid + 1, fin, seuil);
                ecrire_fils(max2, "temp2.txt");
                exit(EXIT_SUCCESS);
            } else { // Processus père
                wait(NULL); // Attente que le fils 1 termine
                lire_pere(&max1, "temp1.txt");
                wait(NULL); // Attente que le fils 2 termine
                lire_pere(&max2, "temp2.txt");

                // Retourne le maximum entre les résultats des fils
                return maxi(max1, max2);
            }
        }
    }
}

int main() {
    int tab[] = {58, 24, 88, 22, 55, 69, 82, 56, 11, 41, 19, 40, 56, 72, 22, 91, 23, 22, 89, 63, 83, 61, 49, 17, 60, 54, 70, 52, 25, 26, 31};
    int debut = 0;
    int fin = 7;
    int seuil = 5;

    printf("L'élément maximum dans le tableau est : %d\n", recherche(tab, debut, fin, seuil));

    return 0;
}
