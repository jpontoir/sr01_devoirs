#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

int maxi(int i, int j){
	if(i>j) return i;
	else return j;
}

int max(int* tab, int debut, int fin){
	int max = tab[debut];
	for(int i=debut+1; i<=fin; i++){
		if(tab[i] > max) max = tab[i];
	}
	return max;
}


int recherche(int* tab, int debut, int fin, int seuil){
	if(fin-debut+1 <= seuil) return max(tab, debut, fin);
	else{
		pid_t pid1, pid2;
		int mid = (debut+fin)/2, res1, res2, maximum, fd[2];
		pipe(fd);
		pid1 = fork();
		
		if(pid1 < 0){
			perror("impossible de créer le fils");
			exit(EXIT_FAILURE);
		}
		else if(pid1 == 0){
			close(fd[0]);
			res1 = recherche(tab, debut, mid, seuil);
			printf("res1 : %d\n", res1);
			write(fd[1], &res1, sizeof(res1));
			close(fd[1]);
			exit(EXIT_SUCCESS);
		}
		else{
			pid2 = fork();
			
			if(pid2 < 0){
				perror("impossible de créer le fils");
				exit(EXIT_FAILURE);
			}
			else if(pid2 == 0){
				close(fd[0]);
				res2 = recherche(tab, mid+1, fin, seuil);
				printf("res2 : %d\n", res2);
				write(fd[1], &res2, sizeof(res2));
				close(fd[1]);
				exit(EXIT_SUCCESS);
			}
			else{
				wait(NULL);
				close(fd[1]);
				read(fd[0], &res1, sizeof(res1));
				read(fd[0], &res2, sizeof(res2));
				printf("res1 : %d\n", res1);
				printf("res2 : %d\n", res2);
				maximum = maxi(res1, res2);
				close(fd[0]);
				return maximum;
			}
		}	
	}
}





int main(){
	int tab[] = {4, 21, 5, 82, 9, 7, 45, 14};
	int debut = 1;
	int fin = 6;
	printf("%d\n", recherche(tab, debut, fin, 2));
}
