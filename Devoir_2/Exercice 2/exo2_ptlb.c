#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

struct sigaction act_fils1, act_pere;
pid_t pid1;
pid_t pid2;

void ecrire_fils(int nb, char* name){
	int fd, test;
	fd = open(name, O_RDWR|O_CREAT|O_TRUNC, 0666);
	write(fd, &nb, sizeof(int));
	lseek(fd, 0, SEEK_SET);
	read(fd, &test, sizeof(int));
	close(fd); 
}


void lire_pere(int* nb, char* name){
	int fd;
	fd = open(name, O_RDWR, 0666);
	read(fd, nb, sizeof(int));
	close(fd);
	if(remove(name)!=0){
		perror("pas réussi à effacer le fichier");
		exit(EXIT_FAILURE); 
	}
}

void fctn_fils1(int signum){
	if(signum == SIGUSR1){
		int n;
		printf("Entrez un nombre entier\n");
		scanf("%d", &n);
		ecrire_fils(n, "test1.txt");
		kill(getppid(), SIGUSR2);
	}
}

void fctn_pere(int signum){
	int *nb = (int*)malloc(sizeof(int));
	if(signum == SIGUSR2){
		lire_pere(nb, "test2.txt");
		printf("Le nombre récupéré dans le fichier 2 est : %d\n", *nb);
		lire_pere(nb, "test1.txt");	
		printf("Le nombre récupéré dans le fichier 1 est : %d\n", *nb);
	}
}


int main(){

	pid1 = fork();
	
	printf("pid 1 : %d\n", pid1);
	
	if(pid1 < 0){
		perror("pas de fils");
		exit(EXIT_FAILURE);
	}
	else if(pid1 == 0){
		act_fils1.sa_handler = fctn_fils1;
		sigaction(SIGUSR1, &act_fils1, NULL);
		pause();
		exit(EXIT_SUCCESS);
	}
	else{
		pid2 = fork();
		
		printf("pid 2 : %d\n", pid2);
		
		if(pid2 < 0){
			perror("pas de fils");
			exit(EXIT_FAILURE);
		}
		else if(pid2 == 0){
			int n;
			printf("Entrez un nombre entier\n");
			scanf("%d", &n);
			ecrire_fils(n, "test2.txt");
			kill(pid1, SIGUSR1);
			exit(EXIT_SUCCESS);
		}
		else{
			act_pere.sa_handler = fctn_pere;
			sigaction(SIGUSR2, &act_pere, NULL);
			pause();
		}
	}
	
	return 0;
}


