#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


void ecrire_fils(int nb, char* name){
	int fd, test;
	printf("%s t\n", name);
	fd = open(name, O_RDWR|O_CREAT|O_TRUNC, 0666);
	write(fd, &nb, sizeof(int));
	lseek(fd, 0, SEEK_SET);
	read(fd, &test, sizeof(int));
	printf("dans le fils : %d\n", test);
	close(fd); 
}


void lire_pere(int* nb, char* name){
	int fd;
	
	if(access(name, F_OK) != -1)
		printf("le fichier existe\n");
	else
		printf("cheh\n");
	
	fd = open(name, O_RDWR, 0666);
	read(fd, &nb, sizeof(int));
	printf("dans le pere : %d\n", nb);
	close(fd);
	remove(name); 
}


int lire(char *chaine, int longueur) // permet de supprimer le \n d'un texte récupérer par un fgets()
{
    char* pos = NULL;
 
    if (fgets(chaine, longueur, stdin) != NULL)  // Test si erreur de saisie
    {
        pos = strchr(chaine, '\n');
        if (pos != NULL)
        {
            *pos = '\0';
        }
        return 1;
    } else{
        return 0;
    }
}



int main(){
	int n;
	int nb;
	char* test;
	char nom[20];
	int fd[2];
	pipe(fd);
	pid_t pid = fork();
	
	if (pid < 0) {
		printf("Erreur, impossible de creer le processus fils\n");
		return -1;
	}
	else if (pid == 0) {
		close(fd[0]);
		printf("Entrez le nom d'un fichier (moins de 20 caracteres)\n");
		lire(test, 20);
		strcpy(nom, test);
		write(fd[1], nom, strlen(nom)+1);
		close(fd[1]);
		printf("Entrez un nombre entier\n");
		scanf("%d", &n);
		printf("%d", n);
		ecrire_fils(n, nom);
	}
	else {
		close(fd[1]);
		wait(NULL);
		read(fd[0], nom, sizeof(nom));
		close(fd[0]);
		printf("le nom : %s\n", nom);
		lire_pere(&nb, nom);
		printf("Le nombre récupéré dans le fichier est : %d\n", nb);
	}
	return 0;
}
