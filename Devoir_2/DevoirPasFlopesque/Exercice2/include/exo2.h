#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

void ecrire_fils(int nb, char* name);
void lire_pere(int* nb, char* name);
void fctn_fils1(int signum);
void fctn_pere(int signum);

