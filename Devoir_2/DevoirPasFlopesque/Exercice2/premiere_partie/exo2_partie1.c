#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>


void ecrire_fils(int nb, char* name){
	int fd, test;
	fd = open(name, O_RDWR|O_CREAT|O_TRUNC, 0666);
	write(fd, &nb, sizeof(int)); // ecrit nb dans le fichier name
	close(fd); 
}


void lire_pere(int* nb, char* name){
	int fd;
	fd = open(name, O_RDWR, 0666);
	read(fd, nb, sizeof(int)); // lit la valeur dans le fichier name et la stocke dans nb
	close(fd);
	if(remove(name)!=0){ // suppression du fichier name
		perror("pas réussi à effacer le fichier");
		exit(EXIT_FAILURE); 
	}
}

int main(){
	int n;
	int *nb;
	pid_t pid = fork();
	
	if (pid < 0) {
		printf("Erreur, impossible de creer le processus fils\n");
		return -1;
	}
	else if (pid == 0) { // dans le fils 
		printf("Entrez un nombre entier\n");
		scanf("%d", &n);
		ecrire_fils(n, "test.txt");
	}
	else { // dans le père
		wait(NULL); // attente de l'execution du fils
		lire_pere(nb, "test.txt");
		printf("Le nombre récupéré dans le fichier est : %d\n", *nb);
	}
	return 0;
}
