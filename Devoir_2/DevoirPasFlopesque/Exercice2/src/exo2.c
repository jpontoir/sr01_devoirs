#include "../include/exo2.h"



void ecrire_fils(int nb, char* name){
	int fd, test;
	fd = open(name, O_RDWR|O_CREAT|O_TRUNC, 0666);
	write(fd, &nb, sizeof(int)); // ecrit la valeur de nb dans le fichier name
	close(fd); 
}


void lire_pere(int* nb, char* name){
	int fd;
	fd = open(name, O_RDWR, 0666);
	read(fd, nb, sizeof(int)); // récupère la valeur dans le fichier name et la stocke dans nb
	close(fd);
	if(remove(name)!=0){ // suppression du fichier name
		perror("pas réussi à effacer le fichier");
		exit(EXIT_FAILURE); 
	}
}

void fctn_fils1(int signum){
	if(signum == SIGUSR1){ // vérification qu'on a bien reçu le bon signal
		int n;
		printf("Entrez un nombre entier\n");
		scanf("%d", &n);
		ecrire_fils(n, "test1.txt");
		kill(getppid(), SIGUSR2); // envoie de SIGUSR2 au processus pere
	}
}

void fctn_pere(int signum){
	int *nb = (int*)malloc(sizeof(int));
	if(signum == SIGUSR2){ // verification qu'on a bien reçu le bon signal
		lire_pere(nb, "test2.txt");
		printf("Le nombre récupéré dans le fichier 2 est : %d\n", *nb);
		lire_pere(nb, "test1.txt");	
		printf("Le nombre récupéré dans le fichier 1 est : %d\n", *nb);
	}
}



