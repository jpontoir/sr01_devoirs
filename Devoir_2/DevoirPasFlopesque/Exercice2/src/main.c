#include "../include/exo2.h"



int main(){
	struct sigaction act_fils1, act_pere;
	pid_t pid1;
	pid_t pid2;
	pid1 = fork();
	if(pid1 < 0){
		perror("pas de fils");
		exit(EXIT_FAILURE);
	}
	else if(pid1 == 0){ // dans le fils 1
		act_fils1.sa_handler = fctn_fils1; // actions a effectuer lors de la reception du signal
		sigaction(SIGUSR1, &act_fils1, NULL); // lorsque SIGUSR1 reçu, execute act_fils1
		pause(); // mise en attente de la reception du signal
		exit(EXIT_SUCCESS);
	}
	else{
		pid2 = fork();
		
		if(pid2 < 0){
			perror("pas de fils");
			exit(EXIT_FAILURE);
		}
		else if(pid2 == 0){ // dans le fils 2
			int n;
			printf("Entrez un nombre entier\n");
			scanf("%d", &n);
			ecrire_fils(n, "test2.txt");
			kill(pid1, SIGUSR1); // envoie du signal SIGUSR1 au fils 1
			exit(EXIT_SUCCESS);
		}
		else{
			act_pere.sa_handler = fctn_pere; // actions a effectuer lors de la reception du signal
			sigaction(SIGUSR2, &act_pere, NULL); // lorsque SIGUSR2 reçu, execute act_pere
			pause(); // mise en attente de la reception d'un signal
		}
	}
	
	return 0;
}
