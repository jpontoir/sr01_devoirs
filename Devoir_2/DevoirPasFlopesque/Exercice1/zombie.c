#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){
	pid_t pid = fork();
	
	if(pid < 0){
		perror("impossible de créer le fils");
		exit(EXIT_FAILURE);
	}
	else if(pid == 0){ // dans le processus fils
		printf("Enfant en cours d'exécution\n");
		sleep(60); // mise en pause du fils
		printf("L'enfant a fini son exécution\n");
		exit(EXIT_SUCCESS);
	}
	else {
		printf("Le père attend l'execution' de son fils\n");
		exit(EXIT_SUCCESS);
	}
	return 0;
	
}
