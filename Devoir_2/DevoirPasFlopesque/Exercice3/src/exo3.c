#include "../include/exo3.h"


int maxi(int i, int j){
	if(i>j) return i;
	else return j;
}

int max(int* tab, int debut, int fin){
	if ((debut < 0) || (debut > fin)) {
		perror("Indices invalides !!");
		exit(EXIT_FAILURE);
	}
	int max = tab[debut];
	for(int i=debut+1; i<=fin; i++){
		if(tab[i] > max) max = tab[i];
	}
	return max;
}


int recherche(int* tab, int debut, int fin, int seuil){
	if(fin-debut+1 <= seuil) return max(tab, debut, fin); // verification de si la taille du tableau est < au seuil
	else{
		pid_t pid1, pid2;
		int mid = (debut+fin)/2, res1, res2, maximum, fd[2];
		pipe(fd);
		pid1 = fork();
		
		if(pid1 < 0){
			perror("impossible de créer le fils");
			exit(EXIT_FAILURE);
		}
		else if(pid1 == 0){ // dans le fils 1
			close(fd[0]);
			res1 = recherche(tab, debut, mid, seuil); // recherche sur la première moitie du tableau
			write(fd[1], &res1, sizeof(res1)); // passage de la valeur par pipe
			close(fd[1]);
			exit(EXIT_SUCCESS);
		}
		else{
			pid2 = fork();
			
			if(pid2 < 0){
				perror("impossible de créer le fils");
				exit(EXIT_FAILURE);
			}
			else if(pid2 == 0){ // dans le fils 2
				close(fd[0]);
				res2 = recherche(tab, mid+1, fin, seuil); // recherche sur la seconde moitie du tableau
				write(fd[1], &res2, sizeof(res2)); // passage de la valeur par pipe
				close(fd[1]);
				exit(EXIT_SUCCESS);
			}
			else{
				wait(NULL);
				close(fd[1]);
				read(fd[0], &res1, sizeof(res1)); // recuperation des valeurs
				read(fd[0], &res2, sizeof(res2));
				maximum = maxi(res1, res2); // recherche du maximum entre les deux elements recuperes
				close(fd[0]);
				return maximum;
			}
		}	
	}
}





int main(){
	int tab[] = {4, 21, 5, 82, 9, 7, 45, 14};
	int debut = 1;
	int fin = 6;
	printf("Le maximum du tableau est %d.\n", recherche(tab, debut, fin, 2));
}
