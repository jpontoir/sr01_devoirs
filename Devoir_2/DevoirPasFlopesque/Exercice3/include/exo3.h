#ifndef EXO3_H
#define EXO3_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>


int maxi(int i, int j);
int max(int* tab, int debut, int fin);
int recherche(int* tab, int debut, int fin, int seuil);


#endif
